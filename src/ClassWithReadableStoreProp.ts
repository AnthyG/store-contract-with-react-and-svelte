import type { Readable, Writable } from 'svelte/store';
import { writable } from 'svelte/store';

export class ClassWithReadableStoreProp {
	private text: string;
	private textStore: Writable<string>;

	constructor(text = 'hi') {
		this.text = text;
		this.textStore = writable(this.text);
	}

	setText(text: string) {
		this.text = text;
		this.textStore.set(this.text);
	}

	getText(): string {
		return this.text;
	}

	getTextStore(): Readable<string> {
		return {
			subscribe: this.textStore.subscribe,
		};
	}
}
