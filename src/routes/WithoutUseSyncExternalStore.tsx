import type { ClassWithReadableStoreProp } from '../ClassWithReadableStoreProp';
import { useEffect, useState } from 'react';

export function WithoutUseSyncExternalStore({
	readableClass,
}: {
	readableClass: ClassWithReadableStoreProp;
}) {
	const [text, setText_] = useState(readableClass.getText());

	useEffect(() => {
		const unsubscribe = readableClass.getTextStore().subscribe((value) => {
			setText_(value);
		});

		return () => unsubscribe();
	}, [readableClass]);

	return (
		<input
			type="text"
			onInput={(event) => readableClass.setText(event.currentTarget.value)}
			value={text}
		/>
	);
}
