import type { ClassWithReadableStoreProp } from '../ClassWithReadableStoreProp';
import { WithoutUseSyncExternalStore } from './WithoutUseSyncExternalStore';
import { WithUseSyncExternalStore } from './WithUseSyncExternalStore';

export function App({ readableClass }: { readableClass: ClassWithReadableStoreProp }) {
	return (
		<>
			<WithoutUseSyncExternalStore readableClass={readableClass} />
			<WithUseSyncExternalStore readableClass={readableClass} />
		</>
	);
}
