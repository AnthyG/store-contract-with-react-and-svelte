import { ClassWithReadableStoreProp } from '../ClassWithReadableStoreProp';
import { useSyncExternalStore } from 'react';

export function WithUseSyncExternalStore({
	readableClass,
}: {
	readableClass: ClassWithReadableStoreProp;
}) {
	const getText = () => readableClass.getText();
	const text = useSyncExternalStore(readableClass.getTextStore().subscribe, getText);

	return (
		<input
			type="text"
			onInput={(event) => readableClass.setText(event.currentTarget.value)}
			value={text}
		/>
	);
}
